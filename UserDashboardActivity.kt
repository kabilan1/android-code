package com.gox.app.ui.dashboard

import android.annotation.SuppressLint
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.google.android.material.bottomnavigation.BottomNavigationItemView
import com.google.android.material.bottomnavigation.BottomNavigationMenuView
import com.google.android.material.bottomnavigation.LabelVisibilityMode
import com.gox.app.R
import com.gox.app.databinding.ActivityHomeBinding
import com.gox.app.ui.home_fragment.HomeFragment
import com.gox.app.ui.myaccount_fragment.MyAccountFragment
import com.gox.app.ui.notification_fragment.NotificationFragment
import com.gox.app.ui.order_fragment.OrderFragment
import com.gox.basemodule.base.BaseActivity


class UserDashboardActivity : BaseActivity<ActivityHomeBinding>() {

    lateinit var mViewDataBinding: ActivityHomeBinding

    private var homeFragment: HomeFragment? = null
    private var orderFragment: OrderFragment? = null
    private var notificationFragment: NotificationFragment? = null
    private var accountFragment: MyAccountFragment? = null

    private var fragmentManager: FragmentManager? = null

    override fun getLayoutId(): Int = R.layout.activity_home

    override fun initView(mViewDataBinding: ViewDataBinding?) {
        this.mViewDataBinding = mViewDataBinding as ActivityHomeBinding

        fragmentManager = supportFragmentManager
        if (homeFragment == null) {
            homeFragment = HomeFragment()
            addFragment(homeFragment!!)
            orderFragment = OrderFragment()
            addFragment(orderFragment!!)
            notificationFragment = NotificationFragment()
            addFragment(notificationFragment!!)
            accountFragment = MyAccountFragment()
            addFragment(accountFragment!!)
        }

        mViewDataBinding.bottomNavigation.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.home_fragment -> {
                    fragmentManager!!.beginTransaction()
                            .hide(orderFragment!!)
                            .hide(notificationFragment!!)
                            .hide(accountFragment!!)
                            .show(homeFragment!!)
                            .commit()
                    true
                }
                R.id.order_fragment -> {
                    fragmentManager!!.beginTransaction()
                            .hide(homeFragment!!)
                            .hide(notificationFragment!!)
                            .hide(accountFragment!!)
                            .show(orderFragment!!)
                            .commit()
                    orderFragment?.goToPastOrder()
                    true

                }
                R.id.notification_fragment -> {
                    fragmentManager!!.beginTransaction()
                            .hide(homeFragment!!)
                            .hide(orderFragment!!)
                            .hide(accountFragment!!)
                            .show(notificationFragment!!)
                            .commit()
                    true
                }
                R.id.myaccount_fragment -> {
                    fragmentManager!!.beginTransaction()
                            .hide(homeFragment!!)
                            .hide(orderFragment!!)
                            .hide(notificationFragment!!)
                            .show(accountFragment!!)
                            .commit()
                    true
                }


                else -> false
            }
        }
        mViewDataBinding.bottomNavigation.selectedItemId = R.id.home_fragment
        removeShifting()

    }

    override fun onResume() {
        super.onResume()
       when (mViewDataBinding.bottomNavigation.selectedItemId){
           R.id.order_fragment ->{
               orderFragment?.pageOnResume()
           }
       }
    }

    private fun addFragment(fragment: Fragment) {
        fragmentManager!!.beginTransaction()
                .add(R.id.main_container, fragment)
                .commit()
    }

    @SuppressLint("RestrictedApi")
    private fun removeShifting() {
        val menuView = mViewDataBinding.bottomNavigation.getChildAt(0) as BottomNavigationMenuView
        for (i in 0 until menuView.childCount) {
            val item: BottomNavigationItemView = menuView.getChildAt(i) as BottomNavigationItemView

            item.setShifting(false)
            item.setLabelVisibilityMode(LabelVisibilityMode.LABEL_VISIBILITY_LABELED)

            // set once again checked value, so view will be updated

            item.setChecked(item.itemData.isChecked)
        }
    }


}